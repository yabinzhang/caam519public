#! /usr/bin/env python
import os
sizes = []
times = []
for k in range (5):
  Nx =10*2**k
  modname = 'perf_test%d' % k
  options = ['-da_grid_x',  str(Nx), '-da_grid_y', str(Nx), '-log_view', ':%s.py:ascii_info_detail' % modname]
 # print ('./bin/ex5.o '+' '.join(options))
  os.system('./ex5 '+' '.join(options))
  perfmod = __import__(modname)
  sizes.append(Nx ** 2)
  times.append(perfmod.Stages['Main Stage']['SNESSolve'][0]['time'])
print zip(sizes , times)

from pylab import legend , plot , loglog , show , title , xlabel , ylabel
plot(sizes , times)
title ( 'SNES ex5 ')
xlabel('Problem Size $N$')
ylabel( 'Time (s)') 
show()
loglog(sizes , times)
title ( 'SNES ex5 ')
xlabel('Problem Size $N$')
ylabel( 'Time (s)' )
show()

