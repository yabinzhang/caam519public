#! /usr/bin/env python
import os
import matplotlib.pyplot as plt

sizes = []
times_ilu = []
times_gamg = []
for k in range (5):
  Nx =10*2**k
  modname_ilu = 'perf_ilu%d' % k
  modname_gamg = 'perf_gamg%d' % k
  options_ilu = ['-ksp_type', 'gmres', '-pc_type',  'ilu', '-da_grid_x',  str(Nx), '-da_grid_y', str(Nx), '-log_view', ':%s.py:ascii_info_detail' % modname_ilu]
  options_gamg= ['-ksp_type', 'gmres', '-pc_type',  'gamg', '-da_grid_x',  str(Nx), '-da_grid_y', str(Nx), '-log_view', ':%s.py:ascii_info_detail' % modname_gamg]
  os.system('./ex5 '+' '.join(options_ilu))
  perfmod_ilu = __import__(modname_ilu)
  os.system('./ex5 '+' '.join(options_gamg))
  perfmod_gamg = __import__(modname_gamg)
  sizes.append(Nx ** 2)
  times_ilu.append(perfmod_ilu.Stages['Main Stage']['KSPSolve'][0]['time'])
  times_gamg.append(perfmod_gamg.Stages['Main Stage']['KSPSolve'][0]['time'])
print zip(sizes , times_ilu, times_gamg)


from pylab import legend , plot , loglog , show , title , xlabel , ylabel
plt.plot(sizes , times_ilu, 'r-',label='GMRES_ilu')
plt.plot(sizes, times_gamg, 'b-',label='GMRES_gamg')
title ( 'ex5 ')
xlabel('Problem Size $N$')
ylabel( 'Time (s)')
plt.legend(loc=2)
plt.savefig('f1LT.png')
plt.show()


plt.loglog(sizes , times_ilu, 'r-',label='GMRES_ilu.')
plt.loglog(sizes, times_gamg, 'b-',label='GMRES_gamg')
title ( 'ex5 loglog ')
xlabel('Problem Size $N$')
ylabel( 'Time (s)' )
plt.legend(loc=2)
plt.savefig('f2LT.png')
plt.show()
 
